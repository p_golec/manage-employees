// script.js

    // create the module and name it scotchApp
        // also include ngRoute for all our routing needs
    var scotchApp = angular.module('scotchApp', ['ngRoute']);

    // configure our routes
    scotchApp.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'pages/home.html',
                controller  : 'mainController'
            })

            // route for the about page
            .when('/about', {
                templateUrl : 'pages/about.html',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'pages/contact.html',
                controller  : 'contactController'
            });
    });

    // create the controller and inject Angular's $scope
    scotchApp.controller('mainController', function($scope) {
        // create a message to display in our view
        $scope.message = 'I am just a home page, go and see my buddies!';
    });

    scotchApp.controller('aboutController', function($scope) {
        $scope.message = 'Look! I am an about page. Here, you can view a list of \n\
  employees and add likes and dislikes for them!\n\
This was developed using Angular JS, fontawesome and bootstrap';
        var employees = [{
          Name: "Mark",
          Surname: "Smith",
          Position: "Baker",
          Salary: 25000,
          Likes: 0,
          Dislikes: 0
        },
        {  Name: "Martha",
          Surname: "Smith",
          Position: "Teacher",
          Salary: 21000,
          Likes: 0,
          Dislikes: 0
        }];
          $scope.employees = employees;    
      $scope.incrementLikes = function (employee) {
        employee.Likes++;
      };
      $scope.incrementDislikes = function (employee) {
        employee.Dislikes ++;
      };
    });
    scotchApp.controller('contactController', function($scope) {
        $scope.message = 'Contact me at kontakt@pawel-golec.pl';
    });